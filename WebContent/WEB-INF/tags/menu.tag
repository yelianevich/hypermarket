﻿<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="HypermarketTags" prefix="hyp" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="resources.messages"/>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="brand" href="#">Equilibrium</a>
			<div class="navbar-content">
				<ul class="nav">
					<li style="height: 40px;">
						<a>
							<form name="homepage" method="POST" action=<c:url value="controller"/>>
								<input type="hidden" name="command" value="homepage" /> 
								<input class="menuFormInput" type="submit" value="<fmt:message key="Homepage" />" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="catalogForm" method="POST" action=<c:url value="controller"/>>
								<input type="hidden" name="command" value="catalog" />
								<input class="menuFormInput" type="submit" value="<fmt:message key="Catalog" />" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="goodsForm" method="POST" action=<c:url value="controller"/>>
								<input type="hidden" name="command" value="showGoods" /> 
								<input type="hidden" name="showGoods" value="showAll" /> 
								<input class="menuFormInput" type="submit" value="<fmt:message key="Goods" />" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="aboutForm" method="POST" action=<c:url value="controller"/>>
								<input type="hidden" name="command" value="about" /> 
								<input class="menuFormInput" type="submit" value="<fmt:message key="About" />" />
							</form>
						</a>
					</li>
				</ul>

				<ul class="nav pull-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="">
							<i class="icon-user"></i> <c:out value="${user.login}"/><b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<!-- TODO tag usage -->
							<hyp:roleUserMenuItem user="${user}">
								<li>
									<a>
										<form name="aboutForm" method="POST" action=<c:url value="controller"/>>
											<input type="hidden" name="command" value="${userCommand}" /> 
											<input class="dropdownFormInput" type="submit" value="<fmt:message key="${userCommandLabel}" />" />
										</form>
									</a>
								</li>
							</hyp:roleUserMenuItem>
							<li>
								<a>
									<form name="aboutForm" method="POST">
										<input type="hidden" name="command" value="logout" /> 
										<input class="dropdownFormInput" type="submit" value="<fmt:message key="Logout" />" />
									</form>
								</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="">
							<i class="icon-font"></i><fmt:message key="changeLanguge" /><b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a>
									<form name="languageForm" method="POST" action=<c:url value="controller"/>>
										<input type="hidden" name="command" value="changeLanguage" /> 
										<input type="hidden" name="language" value="ru" />
										<input class="dropdownFormInput" type="submit" value="Русский" />
									</form>
								</a>
							</li>
							<li>
								<a>
									<form name="languageForm" method="POST" action=<c:url value="controller"/>>
										<input type="hidden" name="command" value="changeLanguage" />
										<input type="hidden" name="language" value="en" />
										<input class="dropdownFormInput" type="submit" value="English" />
									</form>
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
