function validate (loginForm)
{
	var login = document.forms["loginForm"]["login"].value;
	var password = document.forms["loginForm"]["password"].value;
	if (login == null || password == null ||
			login == "" || password == "") {	
		document.getElementById("warning").innerHTML = "Please, type in login information.";
		return false;
	}
	return true;
}
