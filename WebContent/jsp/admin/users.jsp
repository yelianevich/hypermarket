<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="hyp" %>
<!DOCTYPE html>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<fmt:setLocale value="${sessionScope.locale}" />
	<fmt:setBundle basename="resources.messages" />
	<title><fmt:message key="Users" /></title>
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
</head>

<body>
	<div id="wrap">
		<div id="main" class="container clear-top">
			<hyp:menu />
			<div class="container">
				<c:choose>
					<c:when test="${not empty users}" >
						<h2><fmt:message key="RegisteredUsers" /></h2> <hr/>
						<table class="table table-striped">
							<thead>
							    <tr>
							    	<th><fmt:message key="LoginName" /></th>
									<th><fmt:message key="IsAdmin" /></th>
									<th><fmt:message key="Ban" /></th>
									<th></th>
							    </tr>
							</thead>
							<c:forEach var="user" items="${users}" >
								<tr>	
									<td>${user.login}</td>
									<td>${user.isAdmin}</td>
									<td>${user.ban}</td>
									<td>
										<form name="banForm" action="controller" method="POST" >
											<input type="hidden" name="command" value="banning"/>
											<input type="hidden" name="userUidForBan" id="userUidForBan" value="${user.uid}"/>
											<c:choose>
												<c:when test="${not user.ban}">	
													<input id="banButton" class="btn btn-danger" type="submit" value="<fmt:message key="MakeBan" />"/>
												</c:when>
												<c:otherwise>
													<input id="banButton" class="btn btn-success" type="submit" value="<fmt:message key="MakeUnban" />"/>
												</c:otherwise>
											</c:choose> 
										</form>
									</td>
								</tr>
							</c:forEach>
						</table>
					</c:when>
					<c:otherwise>
						<fmt:message key="NotAvailable" />
					</c:otherwise>
				</c:choose>					
			</div>
		</div>
	</div>
	<hyp:footer />
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>