<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ct" %>
<!DOCTYPE html>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="About"/></title>
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
</head>

<body>
	<div id="wrap">
		<div id="main" class="container clear-top">
			<ct:menu />
			<div class="container">
				<c:out value="${hypermarket.description}" escapeXml="false"></c:out>
				<hr />
				<fmt:message key="Email"/>
				<c:out value=": ${hypermarket.email}" /><br />
				<fmt:message key="Phone"/>
				<c:out value=": ${hypermarket.phone}" />
				<hr />
				<form name="chainForm" method="POST" action=<c:url value="controller"/>>
					<input type="hidden" name="command" value="showChain"/>
					<input class="btn btn-info" type="submit" value="<fmt:message key="ShowFullChain"/>" />
				</form>
			</div>
		</div>
	</div>
	<ct:footer />
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>