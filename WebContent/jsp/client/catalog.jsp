<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="Utf-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ct" %>
<!DOCTYPE html>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="Catalog" /></title>
	<link rel="stylesheet" href="css/bootstrap.css?version=1" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
</head>

<body>	
	<div id="wrap">
		<div id="main" class="container clear-top">
			<ct:menu />			
			<div class="container">
				<c:choose>
		        	<c:when test="${not empty departments}" >
		            	<h3><fmt:message key="HypermarketCatalog" /></h3><hr/>
		                <ul class="unstyled">
		                <c:forEach var="department" items="${departments}" >
		                     <li>
		                     	<form name="departmentForm" method="POST" action="controller">
		                        	<input type="hidden" name="command" value="showGoods" />
		                        	<input type="hidden" name="showGoods" value="showByDepartment" />
		                        	<input type="hidden" name="departmentUid" value="${department.uid}" /> 
		                       		<input class="catalogFormInput" id="catalogButton" type="submit" value="${department.name}" />
		                        </form>
		                        <ul class="inline">    
		                             <c:forEach var="category" items="${department.categories}" >
		                                 <li style="height: 30px;">
		                                     <form style="padding-left: 35px;" name="categoryForm" method="POST" action="controller">
		                                          <input type="hidden" name="command" value="showGoods" /> 
		                                          <input type="hidden" name="showGoods" value="showByCategory" /> 
		                                          <input type="hidden" name="categoryUid" value="${category.uid}" />
		                                          <input class="btn btn-link" id="catalogButton" type="submit" value="${category.name}" />
		                                     </form>
		                                 </li>
		                             </c:forEach>
		                        </ul>
		                        <hr />
		                	</li>
		                </c:forEach>
		                </ul>
		              </c:when>
		              <c:otherwise>
		              		<fmt:message key="NotAvailable" />
		              </c:otherwise>
		      </c:choose>
			</div>
		</div>
	</div>
	<ct:footer />
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>