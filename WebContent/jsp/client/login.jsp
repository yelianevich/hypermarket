<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>

<head>
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="LOGIN_TITLE" /></title>
	<link rel="stylesheet" href="css/login-box.css?version=1" type="text/css" />
</head>

<body>
	<div id="login-box">
			<h2><fmt:message key="LOGIN_TITLE" /></h2>
			<form name="loginForm" method="POST" action="controller" onsubmit="return validate()">
				<input type="hidden" name="command" value="login" />  
				<div id="login-box-name"><fmt:message key="LoginName" /><br /></div>
				<div id="login-box-field"><input class="form-login" type="text" name="login" value="" /></div><br />
				<div id="login-box-name"><fmt:message key="LoginPassword" /><br /></div>
				<div id="login-box-field"><input class="form-login" type="password" name="password" value="" /></div><br /> 
				<input id="login-box-button" type="image" value="" src="img/login-btn.png" />
			</form>
			<br/><div id="warning" class="login-box-warning"> </div>
	</div>
	<script type="text/javascript" src="js/validation.js?version=1"></script>
</body>

</html>