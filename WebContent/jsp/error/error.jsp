<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="Error"/></title>
</head>

<body>
	<h3><fmt:message key="Error"/></h3>
	<hr />
	<c:choose>
		<c:when test="${not empty errorMessage}">
			<c:out value="${errorMessage}" />
		</c:when>
		<c:otherwise>
			<fmt:message key="UnknownError"/>
		</c:otherwise>
	</c:choose>
	<hr />
	<a href="<c:url value="controller"/>"><fmt:message key="ReturnBack"/></a>
</body>

</html>