<%@ page language="java" pageEncoding="Utf-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ct" %>
<!DOCTYPE html>

<html>

<head>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="Goods" /></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
</head>

<body>	
	<div id="wrap">
		<div id="main" class="container clear-top">
			<ct:menu />
			<div class="container">
				<c:choose>
					<c:when test="${not empty goodsList}" >
						<h2><fmt:message key="GoodsHypermarket" /></h2> <hr/>
						<table class="table table-striped">
							<thead>
							    <tr>
							    	<th><fmt:message key="BarCode" /></th>
									<th><fmt:message key="Name" /></th>
									<th><fmt:message key="Price" /></th>
									<th><fmt:message key="Quantity" /></th>
									<th><fmt:message key="Discount" /></th>
									<th><fmt:message key="Made" /></th>
									<th><fmt:message key="ShelfLife" /></th>
									<th><fmt:message key="MadeIn" /></th>
									<th><fmt:message key="Department" /></th>
									<th><fmt:message key="Category" /></th>
							    </tr>
							</thead>
							<c:forEach var="goodsItem" items="${goodsList}" >
								<tr>	
									<td>${goodsItem.barCode}</td>
									<td>${goodsItem.name}</td>
									<td><fmt:formatNumber value="${goodsItem.price}" /></td>
									<td>${goodsItem.quantity}</td>
									<td>${goodsItem.discount}</td>
									<td><fmt:formatDate value="${goodsItem.madeDate}" type="date"/></td>
									<td>${goodsItem.shelfLife}</td>
									<td>${goodsItem.country.name}</td>
									<td>${goodsItem.department.name}</td>
									<td>${goodsItem.category.name}</td>
								</tr>
							</c:forEach>
						</table>
					</c:when>
					<c:otherwise>
						<fmt:message key="NotAvailable" />
					</c:otherwise>
				</c:choose>					
			</div>
		</div>
	</div>
	<ct:footer />
	
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</body>

</html>