<%@ page language="java" pageEncoding="Utf-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>

<body>
	<div id="header-wrapper">
		<div id="header">
			<div id="logo">
				<c:url value="controller" var="showGoods"><c:param name="command" value="main"></c:param>  </c:url>
				<h1><a href="#">Hypermarket</a></h1>
				<p>design by <a href="http://www.freecsstemplates.org/">FCT</a></p>
			</div>
		</div>
	</div>
</body>

</html>