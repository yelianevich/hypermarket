<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="brand" href="">Equilibrium</a>
			<div class="navbar-content">
				<ul class="nav">
					<li style="height: 40px;">
						<a>
							<form name="homepage" method="POST" action="controller">
								<input type="hidden" name="command" value="homepage" /> 
								<input class="menuFormInput" type="submit" value="Homepage" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="catalogForm" method="POST" action="controller">
								<input type="hidden" name="command" value="catalog" />
								<input class="menuFormInput" type="submit" value="Catalog" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="goodsForm" method="POST" action="controller">
								<input type="hidden" name="command" value="showGoods" /> 
								<input type="hidden" name="showGoods" value="showAll" /> 
								<input class="menuFormInput" type="submit" value="Goods" />
							</form>
						</a>
					</li>
					<li style="height: 40px;">
						<a>
							<form name="aboutForm" method="POST" action="controller">
								<input type="hidden" name="command" value="about" /> 
								<input class="menuFormInput" type="submit" value="About" />
							</form>
						</a>
					</li>
					
				</ul>

				<ul class="nav pull-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="">
							<i class="icon-user"></i> <c:out value="${user.login}"/><b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#">Edit my details</a></li>
							<li><a href="#">Order history</a></li>
							<li class="divider"></li>
							<li><a href="#">Log out</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/bootstrap.min.js"><jsp:text /></script>	
