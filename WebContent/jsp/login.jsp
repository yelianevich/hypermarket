<%@ page language="java" pageEncoding="Utf-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>

<head>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="LOGIN_TITLE" /></title>
	<link rel="stylesheet" href="css/login-box.css?version=1" type="text/css" />
</head>

<body>
	<div id="login-box">
			<h2><fmt:message key="LOGIN_TITLE" /></h2>
			<form name="loginForm" method="POST" action="controller">
				<input type="hidden" name="command" value="login" />  
				<div id="login-box-name"><fmt:message key="LoginName" /><br /></div>
				<div id="login-box-field"><input class="form-login" type="text" name="login" value="" /></div><br />
				<div id="login-box-name"><fmt:message key="LoginPassword" /><br /></div>
				<div id="login-box-field"><input class="form-login" type="password" name="password" value="" /></div><br /> 
				<input id="login-box-button" type="image" value="" src="img/login-btn.png" />
			</form>
	</div>
</body>

</html>