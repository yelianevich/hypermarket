<%@ page language="java" pageEncoding="Utf-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>

<head>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="resources.messages"/>
	<title><fmt:message key="MainTitle"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<link rel="stylesheet" href="css/bootstrap.css?version=13" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
</head>

<body>
	<div id="wrap">
		<div id="main" class="container clear-top">
			<ct:menu />
			<div class="container">
				<div class="hero-unit">
					<h1><fmt:message key="Hypermarket"/> Equilibrium</h1>
					<p><fmt:message key="Slogan"/></p>
					<p></p>
				</div>
			</div>
		</div>
	</div>
	<ct:footer />
	<script type="text/javascript" src="js/jquery-1.8.3.js"><jsp:text /></script>
	<script type="text/javascript" src="js/bootstrap.js"><jsp:text /></script>
</body>

</html>