package by.bsuir.hypermarket.command;

import java.io.IOException;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.request.CommandParams;

/**
 * Common interface for every command in the application 
 * @author Raman
 *
 */
public interface Command {
	
	/**
	 * Method processes request and returns corresponding page path
	 * @param request
	 * @return path to the requested page
	 * @throws ServletException
	 * @throws IOException
	 */
	public String execute(CommandParams<?> request) throws ServletException, IOException;
}