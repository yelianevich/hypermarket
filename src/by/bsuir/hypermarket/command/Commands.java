package by.bsuir.hypermarket.command;

/**
 * Defines commands and corresponding attributes names
 * @author Raman
 *
 */
public final class Commands {
	public final static String COMMAND = "command";
	
	public final static String HOMEPAGE_COMMAND = "homepage";
	
	public final static String LOGOUT_COMMAND = "logout";
	
	public final static String LOGIN_COMMAND = "login";
	public final static String LOGIN_LOGIN = "login";
	public final static String LOGIN_PASSWORD = "password";
	public final static String LOGIN_USER = "user";
	
	public final static String ABOUT_COMMAND = "about";
	public final static String ABOUT_HYPERMARKET = "hypermarket";
	public final static String ABOUT_HYPERMARKETS = "hypermarkets";
	public final static String ABOUT_CHAIN = "showChain";
	
	public final static String CATALOG_COMMAND = "catalog";
	public final static String CATALOG_DEPARTMENTS = "departments";
	
	public final static String SHOW_GOODS_COMMAND = "showGoods";
	public final static String SHOW_GOODS_BY_CATEGORY = "showByCategory";
	public final static String SHOW_GOODS_BY_DEPARTMENT = "showByDepartment";
	public final static String SHOW_GOODS_CATEGOTY = "categoryUid";
	public final static String SHOW_GOODS_DEPARTMENT = "departmentUid";
	public final static String SHOW_GOODS_LIST = "goodsList";
	
	public final static String CHANGE_LANG_COMMAND = "changeLanguage";
	public final static String CHANGE_LANG_LANG = "language";
	public final static String CHANGE_LANG_RU = "ru";
	public final static String CHANGE_LANG_RUSSIAN = "ru_RU";
	public final static String CHANGE_LANG_EN = "en";
	public final static String CHANGE_LANG_ENGLISH = "en_GB";
	public final static String CHANGE_LANG_LOCALE = "locale";
	public final static String CHANGE_LANG_PREV_COMMAND = "previousCommand";
	
	public final static String SHOW_USERS_COMMAND = "showUsers";
	public final static String SHOW_USERS_LIST = "users";
	public final static String SHOW_USERS_TAG_MENU_COMMAND = "userCommand";
	public final static String SHOW_USERS_TAG_MENU_LABEL = "userCommandLabel";
	
	public final static String INVERT_BAN_COMMAND = "banning";
	public final static String INVERT_BAN_USER_UID= "userUidForBan";
	
	public final static String ERROR_MESSAGE= "errorMessage";
}
