package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.concrete.HypermarketDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Hypermarket;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that shows information about the whole chain of hypermarkets
 * @author Raman
 *
 */
public class AboutChainCommand implements Command {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = null;
		AbstractDao<Hypermarket> hypermarketDao = new HypermarketDao();
		try {
			List<Hypermarket> hypermarkets = hypermarketDao.findAll();
			request.setAttribute(Commands.ABOUT_HYPERMARKETS, hypermarkets);
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ABOUT_CHAIN_PAGE_PATH);
		} catch (DaoException e) {
			log.error("Was not able to retrieve current hypermarket information", e);
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}

}
