package by.bsuir.hypermarket.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.concrete.HypermarketDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Hypermarket;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that shows information about current hypermarket
 * @author Raman
 *
 */
public class AboutCommand implements Command {
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = null;
		HypermarketDao hypermarketDao = new HypermarketDao();
		try {
			String strId = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.CURRENT_HYPERMARKET_ID);
			int intId = Integer.parseInt(strId);
			Hypermarket hypermarket = hypermarketDao.findEntityById(intId);
			request.setAttribute(Commands.ABOUT_HYPERMARKET, hypermarket);
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ABOUT_PAGE_PATH);
		} catch (DaoException e) {
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}