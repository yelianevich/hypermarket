package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.concrete.DepartmentDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Department;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that shows catalog of available goods in hypermarket
 * @author Raman
 *
 */
public class CatalogCommand implements Command {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = null;
		AbstractDao<Department> departmentDao = new DepartmentDao();
		try {
			List<Department> departments = departmentDao.findAll();
			request.setAttribute(Commands.CATALOG_DEPARTMENTS, departments);
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.CATALOG_PAGE_PATH);
		} catch (DaoException e) {
			log.error("Was not able to execute catalog command");
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}
