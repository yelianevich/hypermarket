package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

public class ChangeLanguageCommand implements Command {

	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.MAIN_PAGE_PATH);
		String language = request.getParameter(Commands.CHANGE_LANG_LANG);
		if (language != null) {
			switch (language) {
			case Commands.CHANGE_LANG_RU :
				String localeStrRu = Commands.CHANGE_LANG_RUSSIAN;
				request.setSessionAttribute(Commands.CHANGE_LANG_LOCALE, localeStrRu);
				break;
			case Commands.CHANGE_LANG_EN :
				String localeStrEn = Commands.CHANGE_LANG_ENGLISH;
				request.setSessionAttribute(Commands.CHANGE_LANG_LOCALE, localeStrEn);
				break;
			default :
				Locale defaultLocale = Locale.getDefault();
				String localeStrDef = defaultLocale.getLanguage() + "_" + defaultLocale.getCountry();
				request.setSessionAttribute(Commands.CHANGE_LANG_LOCALE, localeStrDef);
				break;
			}
		}
		return page;
	}

}
