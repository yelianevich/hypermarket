package by.bsuir.hypermarket.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that shows hypermarket website homepage
 * @author Raman
 *
 */
public class HomepageCommand implements Command {

	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.MAIN_PAGE_PATH);
		return page;
	}

}
