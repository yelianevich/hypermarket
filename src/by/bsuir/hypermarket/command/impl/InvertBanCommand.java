package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.concrete.UserDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.User;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

public class InvertBanCommand implements Command {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		try {	
			int userUid = Integer.parseInt(request.getParameter(Commands.INVERT_BAN_USER_UID));
			UserDao userDao = new UserDao();
			User userToBan = userDao.findEntityById(userUid);
			if (userToBan.getLogin() != null) {
				userToBan.setBan(!userToBan.isBan());
				userDao.updateEntity(userToBan);
				List<User> users = userDao.findAll();
				request.setAttribute(Commands.SHOW_USERS_LIST, users);
			}
		} catch (NumberFormatException e) {
			log.error("Can not parse user id", e);
		} catch (DaoException e) {
			log.error("Was not able to get user's data", e);
		}
		
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.SHOW_USERS_PAGE_PATH);
		return page;
	}

}
