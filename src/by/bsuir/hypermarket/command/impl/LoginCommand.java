package by.bsuir.hypermarket.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.concrete.UserDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.User;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.manager.MessageManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that checks login and password. Than it returns homepage path 
 * if login and password is valid or error page otherwise  
 * @author Raman
 *
 */
public class LoginCommand implements Command {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());

	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = null;
		String login = request.getParameter(Commands.LOGIN_LOGIN);
		String password = request.getParameter(Commands.LOGIN_PASSWORD);
		try {	
			UserDao userDao = new UserDao();
			User user = userDao.findUser(login, password);
			if (user.getLogin() != null) {
				if (!user.isBan()) {
					request.setSessionAttribute(Commands.LOGIN_USER, user);
					request.setSessionAttribute(Commands.CHANGE_LANG_LOCALE, Commands.CHANGE_LANG_ENGLISH);
					page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.MAIN_PAGE_PATH);
				} else {
					request.setAttribute(Commands.ERROR_MESSAGE, MessageManager.INSTANCE.getProperty(
							MessageManager.USER_BANNED_MESSAGE));
					page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
				}
			} else {
				request.setAttribute(Commands.ERROR_MESSAGE, MessageManager.INSTANCE.getProperty(
						MessageManager.LOGIN_ERROR_MESSAGE));
				page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
			}
		} catch (DaoException daoException) {
			log.error("Was not able to execute login command");
			request.setAttribute(Commands.ERROR_MESSAGE, MessageManager.INSTANCE.getProperty(
					MessageManager.DAO_ERROR_MESSAGE));
			page = ConfigurationManager.INSTANCE.getProperty(
					ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}
}
