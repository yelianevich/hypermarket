package by.bsuir.hypermarket.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that logout current user and returns login page path
 * @author Raman
 *
 */
public class LogoutCommand implements Command {

	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		request.destroySession();
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
		return page;
	}
}
