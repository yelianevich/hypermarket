package by.bsuir.hypermarket.command.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that defines behaviour if command parameter is missing
 * @author Raman
 *
 */
public class NoCommand implements Command {
	
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
		return page;
	}
}