package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.dao.concrete.HypermarketDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Hypermarket;
import by.bsuir.hypermarket.manager.ConfigurationManager;

public class ShowChainCommand implements Command {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String page = null;
		HypermarketDao hypermarketDao = new HypermarketDao();
		try {
			List<Hypermarket> hypermarkets = hypermarketDao.findAll();
			request.setAttribute("hypermarkets", hypermarkets);
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ABOUT_CHAIN_PAGE_PATH);
		} catch (DaoException e) {
			page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.ERROR_PAGE_PATH);
		}
		return page;
	}

}
