package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.concrete.GoodsDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Goods;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

/**
 * Command that shows all available goods in hypermarket
 * @author Raman
 *
 */
public class ShowGoodsCommand implements Command{
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException, IOException {
		String page = null;		
		String showGoodsType = request.getParameter(Commands.SHOW_GOODS_COMMAND);
		
		List<Goods> goodsList = null;
		try {
			switch (showGoodsType) {
			case Commands.SHOW_GOODS_BY_CATEGORY :
				GoodsDao goodsDao = new GoodsDao();
				int categoryUid = Integer.parseInt(request.getParameter(Commands.SHOW_GOODS_CATEGOTY));
				goodsList = goodsDao.findEntitiesByCategory(categoryUid);
				break;
			case Commands.SHOW_GOODS_BY_DEPARTMENT :
				GoodsDao goodsDepDao = new GoodsDao();
				int departmentUid = Integer.parseInt(request.getParameter(Commands.SHOW_GOODS_DEPARTMENT));
				goodsList = goodsDepDao.findEntitiesByDepartment(departmentUid);
				break;
			default:
				AbstractDao<Goods> abstrGoodsDao = new GoodsDao();
				goodsList = abstrGoodsDao.findAll();		
				break;
			}
		} catch (DaoException e) {
			log.error("Was not able to get goods data", e);
		}	
		request.setAttribute(Commands.SHOW_GOODS_LIST, goodsList);
		page = ConfigurationManager.INSTANCE
				.getProperty(ConfigurationManager.GOODS_PAGE_PATH);
		return page;
	}

}