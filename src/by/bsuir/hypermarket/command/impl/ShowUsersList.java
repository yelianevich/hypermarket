package by.bsuir.hypermarket.command.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.concrete.UserDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.User;
import by.bsuir.hypermarket.manager.ConfigurationManager;
import by.bsuir.hypermarket.request.CommandParams;

public class ShowUsersList implements Command {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	
	@Override
	public String execute(CommandParams<?> request) throws ServletException,
			IOException {
		List<User> users = null;
		AbstractDao<User> usersDao = new UserDao();
		try {
			users = usersDao.findAll();
			request.setAttribute(Commands.SHOW_USERS_LIST, users);
		} catch (DaoException e) {
			log.error("Was not able to get users data", e);
		}
		String page = ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.SHOW_USERS_PAGE_PATH);
		return page;
	}
}
