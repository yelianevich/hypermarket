package by.bsuir.hypermarket.connection.exception;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.exception.HypermarketException;

/**
 * Checked exception thrown when something is wrong in {@link by.bsuir.hypermarket.connection.DbConnectionPool}.
 * Check underlying exception for more details.
 * @author Raman
 *
 */
public class ConnectionPoolException extends HypermarketException {

	private static final long serialVersionUID = 6890070980400263853L;
	private static final String MESSAGE = "Error during operation with connection pool";
	private final Logger log = Logger.getLogger(getClass().getSimpleName());

	public ConnectionPoolException() {
		log.error(MESSAGE);
	}
	
	public ConnectionPoolException(String message) {
		super(message);
		log.error(message);
	}
	
	public ConnectionPoolException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}
	
	public ConnectionPoolException(Throwable cause) {
		super(cause);
		log.error(MESSAGE);
	}
}
