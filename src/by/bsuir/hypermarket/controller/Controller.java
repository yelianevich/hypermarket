package by.bsuir.hypermarket.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.request.CommandParams;
import by.bsuir.hypermarket.request.RequestCommandsParams;
import by.bsuir.hypermarket.request.RequestHelper;

/**
 * Application Controller
 * @author Raman
 *
 */
@WebServlet (
		name = "controller",
		displayName = "hypermarket",
		urlPatterns = "/controller",
		description = "Application Controller"
)
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = -3528652920110865101L;
	
	public Controller() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException {
		CommandParams<HttpServletRequest> commandsParams = new RequestCommandsParams();
		commandsParams.setCommandParamsSource(request);
		
		Command command = RequestHelper.INSTANCE.getCommand(commandsParams);
		String page = command.execute(commandsParams);
		
		request = commandsParams.getCommandParamsSource();
		response.encodeRedirectURL(page);
		RequestDispatcher dispatcher = request.getRequestDispatcher(page);		
		dispatcher.forward(request, response);
	}
}