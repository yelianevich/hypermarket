package by.bsuir.hypermarket.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.command.impl.AboutCommand;
import by.bsuir.hypermarket.command.impl.CatalogCommand;
import by.bsuir.hypermarket.command.impl.ChangeLanguageCommand;
import by.bsuir.hypermarket.command.impl.HomepageCommand;
import by.bsuir.hypermarket.command.impl.LoginCommand;
import by.bsuir.hypermarket.command.impl.LogoutCommand;
import by.bsuir.hypermarket.command.impl.NoCommand;
import by.bsuir.hypermarket.command.impl.AboutChainCommand;
import by.bsuir.hypermarket.command.impl.ShowGoodsCommand;

/**
 * Singleton class. Helps to get command class according to the request {@link Commands#COMMAND} parameter.
 * @author Raman
 *
 */
public enum RequestHelper {
	INSTANCE;
	
	private HashMap<String, Command> commands;
	
	private RequestHelper() {
		commands = new HashMap<String, Command>();
		commands.put(Commands.LOGIN, new LoginCommand());
		commands.put(Commands.SHOW_GOODS, new ShowGoodsCommand());
		commands.put(Commands.HOMEPAGE, new HomepageCommand());
		commands.put(Commands.CATALOG, new CatalogCommand());
		commands.put(Commands.ABOUT, new AboutCommand());
		commands.put(Commands.ABOUT_CHAIN, new AboutChainCommand());
		commands.put(Commands.LOGOUT, new LogoutCommand());
		commands.put(Commands.CHANGE_LANG, new ChangeLanguageCommand());
		
	}

	/**
	 * Determine command in the request and return concrete
	 * Command implementation
	 * @param request
	 * @return
	 */
	public Command getCommand(HttpServletRequest request) {
		String action = request.getParameter(Commands.COMMAND);
		Command command = commands.get(action);
		if (command == null) {
			command = new NoCommand();
		}
		return command;
	}
}
