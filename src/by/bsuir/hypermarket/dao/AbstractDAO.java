package by.bsuir.hypermarket.dao;

import java.util.List;

import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Entity;

/**
 * Defines common operations with data source
 * @author Raman
 *
 * @param <T>
 */
public abstract class AbstractDao <T extends Entity> {
	
	/**
	 * Retrieves and returns all entities from the data source
	 * @return list with entities
	 * @throws DaoException
	 */
	public abstract List<T> findAll() throws DaoException;
	
	/**
	 * Retrieves and returns entity from the data source with id
	 * @param id - entity id to serch for
	 * @return concrete entity
	 * @throws DaoException
	 */
	public abstract T findEntityById(int id) throws DaoException;
	
}