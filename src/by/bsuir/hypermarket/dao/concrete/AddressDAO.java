package by.bsuir.hypermarket.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DbConnection;
import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Address;
import by.bsuir.hypermarket.entity.Country;

public class AddressDao extends AbstractDao<Address> {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String SQL_FIND_ALL = "SELECT `a_uid`, `a_c_uid`, `a_region`, `a_district`, `a_street`, "
			+ "`a_zip_code`, `a_building`, `a_appartment`, "
			+ "`a_house_num`, `country_name`"
			+ "FROM `address` LEFT JOIN `country` ON `country_uid` = `a_c_uid`";
	private static final String SQL_FIND_BY_ID = "SELECT `a_uid`, `a_c_uid`, `a_region`, `a_district`, `a_street`, `a_zip_code`, "
			+ "`a_building`, `a_appartment`, `a_house_num`, `country_name` "
			+ "FROM `address` "
			+ "LEFT JOIN `country` ON `country_uid` = `a_c_uid`"
			+ "WHERE `a_uid` = ?";

	@Override
	public List<Address> findAll() throws DaoException {
		List<Address> addresses = new ArrayList<Address>();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)){
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Address address = new Address();
				fillUpAddress(resultSet, address);
				addresses.add(address);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		} catch (SQLException e) {
			log.error("Error during searching all address entities", e);
			throw new DaoException(e);
		}

		return addresses;
	}

	@Override
	public Address findEntityById(int id) throws DaoException {
		Address address = new Address();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement idStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
			idStatement.setInt(1, id);
			ResultSet resultSet = idStatement.executeQuery();
			if (resultSet.next()) {
				fillUpAddress(resultSet, address);
			}
		} catch (SQLException e) {
			log.error("Error during address entity searching by id", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return address;
	}
	
	private void fillUpAddress(ResultSet resultSet, Address address) throws SQLException {
		address.setUid(resultSet.getInt("a_uid"));
		address.setAppartment(resultSet.getInt("a_appartment"));
		address.setBuilding(resultSet.getInt("a_building"));
		Country country = new Country();
		country.setName(resultSet.getString("country_name"));
		country.setUid(resultSet.getInt("a_c_uid"));
		address.setCountry(country);
		address.setDistrict(resultSet.getString("a_district"));
		address.setHouseNumber(resultSet.getInt("a_house_num"));
		address.setRegion(resultSet.getString("a_region"));
		address.setStreet(resultSet.getString("a_street"));
		address.setZipCode(resultSet.getInt("a_zip_code"));
	}
}
