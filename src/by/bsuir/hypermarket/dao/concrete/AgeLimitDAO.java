package by.bsuir.hypermarket.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DbConnection;
import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.AgeLimit;

public class AgeLimitDao extends AbstractDao<AgeLimit> {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String FIND_ALL = "SELECT `al_uid`, `al_name`, `al_min_age`, `al_description` FROM `age_limit`";
	private static final String FIND_BY_ID = "SELECT `al_uid`, `al_name`, `al_min_age`, `al_description` FROM `age_limit` WHERE `al_uid` = ?";

	@Override
	public List<AgeLimit> findAll() throws DaoException {
		List<AgeLimit> ageLimits = new ArrayList<AgeLimit>();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					AgeLimit ageLimit = new AgeLimit();
					fillUpAgeLimit(resultSet, ageLimit);
				}
			}
		} catch (SQLException e) {
			log.error("Error during searching all age limit entities", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return ageLimits;
	}

	@Override
	public AgeLimit findEntityById(int id) throws DaoException {
		AgeLimit ageLimit = new AgeLimit();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement idStatement = connection.prepareStatement(FIND_BY_ID)) {
			idStatement.setInt(1, id);
			try (ResultSet resultSet = idStatement.executeQuery()) {
				if (resultSet.next()) {
					fillUpAgeLimit(resultSet, ageLimit);
				}
			}
		} catch (SQLException e) {
			log.error("Error during serching AgeLimit entity by id", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return ageLimit;
	}
	
	private void fillUpAgeLimit(ResultSet resultSet, AgeLimit ageLimit) throws SQLException {
		ageLimit.setUid(resultSet.getInt("al_uid"));
		ageLimit.setName(resultSet.getString("al_name"));
		ageLimit.setMinAge(resultSet.getInt("al_min_age"));
		ageLimit.setDescription(resultSet.getString("al_description"));
	}

}
