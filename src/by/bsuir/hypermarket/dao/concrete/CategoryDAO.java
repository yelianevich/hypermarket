package by.bsuir.hypermarket.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DbConnection;
import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Category;

public class CategoryDao extends AbstractDao<Category> {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String SQL_FIND_ALL = "SELECT `c_uid`, `c_name`, `c_description`, "
			+ "`c_department_uid`" + "FROM `category`";
	private static final String SQL_FIND_BY_ID = "SELECT `c_uid`, `c_name`, `c_description`, `c_department_uid`"
			+ "FROM `category` WHERE `c_uid` = ?";
	private static final String SQL_FIND_BY_DEPARTMENT = "SELECT `c_uid`, `c_name`, `c_description`, "
			+ "`c_department_uid`"
			+ "FROM `category`"
			+ "WHERE `c_department_uid` = ?";

	@Override
	public List<Category> findAll() throws DaoException {
		List<Category> categories = new ArrayList<Category>();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
			ResultSet resultSet = statement.executeQuery()) {
			while (resultSet.next()) {
				Category category = new Category();
				fillUpCategory(resultSet, category);
				categories.add(category);
			}
		} catch (SQLException e) {
			log.error("Error during searching all entities", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return categories;
	}

	@Override
	public Category findEntityById(int id) throws DaoException {
		Category category = new Category();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement idStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
			idStatement.setInt(1, id);
			ResultSet resultSet = idStatement.executeQuery();
			if (resultSet.next()) {
				fillUpCategory(resultSet, category);
			}
		} catch (SQLException e) {
			log.error("Error during serching entity by id", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return category;
	}

	public List<Category> findEntitiesByDepartment(int department_uid)
			throws DaoException {
		List<Category> categories = new ArrayList<>();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement idStatement = connection.prepareStatement(SQL_FIND_BY_DEPARTMENT)) {
			idStatement.setInt(1, department_uid);
			ResultSet resultSet = idStatement.executeQuery();
			while (resultSet.next()) {
				Category category = new Category();
				fillUpCategory(resultSet, category);
				categories.add(category);
			}
		} catch (SQLException e) {
			log.error("Error during statement creation or execution", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return categories;
	}

	private void fillUpCategory(ResultSet resultSet, Category category) throws SQLException {
		category.setUid(resultSet.getInt("c_uid"));
		category.setName(resultSet.getString("c_name"));
		category.setDescription(resultSet.getString("c_description"));
	}
}
