package by.bsuir.hypermarket.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DbConnection;
import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.GoodsCondition;

public class GoodsConditionDao extends AbstractDao<GoodsCondition> {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String FIND_ALL = "SELECT `gc_uid`, `gc_name`, `gc_description` FROM `goods_condition`";
	private static final String FIND_BY_ID = FIND_ALL + " WHERE `gc_uid` = ?"; 
	
	@Override
	public List<GoodsCondition> findAll() throws DaoException {
		List<GoodsCondition> conditions = new ArrayList<GoodsCondition>();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				GoodsCondition goodsCondition = new GoodsCondition();
				fillUpGoodsCondition(resultSet, goodsCondition);						
				conditions.add(goodsCondition);
			}
		} catch (SQLException e) {
			log.error("Error during statement creation", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return conditions;
	}

	@Override
	public GoodsCondition findEntityById(int id) throws DaoException {
		GoodsCondition goodsCondition = new GoodsCondition();
		try (DbConnection connection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement idStatement = connection.prepareStatement(FIND_BY_ID)) {
			idStatement.setInt(1, id);
			ResultSet resultSet = idStatement.executeQuery();
			if (resultSet.next()) {
				fillUpGoodsCondition(resultSet, goodsCondition);
			}
		} catch (SQLException e) {
			log.error("Error during statement creation", e);
			throw new DaoException(e);
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		}
		return goodsCondition;
	}
	
	private void fillUpGoodsCondition(ResultSet resultSet, GoodsCondition goodsCondition) throws SQLException {
		goodsCondition.setUid(resultSet.getInt("gc_uid"));
		goodsCondition.setName(resultSet.getString("gc_name"));
		goodsCondition.setDescription(resultSet.getString("gc_description"));
	}
}
