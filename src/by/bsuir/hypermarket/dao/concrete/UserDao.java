package by.bsuir.hypermarket.dao.concrete;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DbConnection;
import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.dao.AbstractDao;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.User;
import by.bsuir.hypermarket.security.SHA1;

public class UserDao extends AbstractDao<User> {
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String SQL_FIND_ALL_USERS = "SELECT `u_uid`, `u_login`, `u_is_admin`, `u_ban` FROM `user`";
	private static final String SQL_FIND_USER = SQL_FIND_ALL_USERS + " WHERE `u_login` = ? AND `u_password` = ?";
	private static final String SQL_UPDATE_USER = "UPDATE `user` SET `u_login` = ?, `u_is_admin` = ?, `u_ban` = ? WHERE `u_uid` = ?";
	private static final String SQL_USER_BY_ID = SQL_FIND_ALL_USERS + " WHERE `u_uid` = ?";
	
	@Override
	public List<User> findAll() throws DaoException {
		List<User> users = new ArrayList<>();
		try (DbConnection dbConnection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement preparedStatement = dbConnection.prepareStatement(SQL_FIND_ALL_USERS)) {	
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				fillUpUser(resultSet, user);
				users.add(user);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		} catch (SQLException e) {
			log.error("Error during searching all address entities", e);
			throw new DaoException(e);
		}
		return users;
	}
	
	@Override
	public User findEntityById(int uid) throws DaoException {
		User user = new User();
		try (DbConnection dbConnection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement preparedStatement = dbConnection.prepareStatement(SQL_USER_BY_ID)) {
			preparedStatement.setInt(1, uid);	
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				fillUpUser(resultSet, user);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		} catch (SQLException e) {
			log.error("Error during searching all address entities", e);
			throw new DaoException(e);
		}
		return user;
	}
	
	/**
	 * Find user according to the specified login and password
	 * @param login user's login
	 * @param password user's password
	 * @return user entity
	 * @throws DaoException
	 */
	public User findUser(String login, String password) throws DaoException {
		User user = new User();
		try (DbConnection dbConnection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement preparedStatement = dbConnection.prepareStatement(SQL_FIND_USER)) {
			preparedStatement.setString(1, login);
			String passwordDigest = SHA1.getHash(password.getBytes());
			preparedStatement.setString(2, passwordDigest);	
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				fillUpUser(resultSet, user);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		} catch (SQLException e) {
			log.error("Error during searching all address entities", e);
			throw new DaoException(e);
		}
		return user;
	}
	
	/**
	 * Update user's data
	 * @param user - user's data to be updated
	 * @return previous user - previous user's data   
	 * @throws DaoException
	 */
	public User updateEntity(User user) throws DaoException {
		User prevUser = new User();
		try (DbConnection dbConnection = DbConnectionPool.INSTANCE.getConnection();
				PreparedStatement findUserStatement = dbConnection.prepareStatement(SQL_USER_BY_ID);
				PreparedStatement preparedStatement = dbConnection.prepareStatement(SQL_UPDATE_USER)) {
			findUserStatement.setInt(1, user.getUid());
			ResultSet resultSet = findUserStatement.executeQuery();
			if (resultSet.next()) {
				fillUpUser(resultSet, prevUser);
			}
			
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.setBoolean(2, user.getIsAdmin());
			preparedStatement.setBoolean(3, user.isBan());
			preparedStatement.setInt(4, user.getUid());

			preparedStatement.executeUpdate();
			if (resultSet.next()) {
				fillUpUser(resultSet, prevUser);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during getting connection from the pool", e);
			throw new DaoException(e);
		} catch (SQLException e) {
			log.error("Error during searching all address entities", e);
			throw new DaoException(e);
		}
		return prevUser;
	}
	
	private void fillUpUser(ResultSet resultSet, User user) throws SQLException {
		user.setUid(resultSet.getInt("u_uid"));
		user.setLogin(resultSet.getString("u_login"));
		user.setIsAdmin(resultSet.getBoolean("u_is_admin"));
		user.setBan(resultSet.getBoolean("u_ban"));
	}
}
