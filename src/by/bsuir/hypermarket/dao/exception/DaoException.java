package by.bsuir.hypermarket.dao.exception;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.exception.HypermarketException;

/**
 * Checked exception thrown when something is wrong with DAO implementation object
 * @author Raman
 *
 */
public class DaoException extends HypermarketException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1202281702878971943L;
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String MESSAGE = "Error during access to object's data";
	
	public DaoException() {
		log.error(MESSAGE);
	}
	
	public DaoException(String message) {
		super(message);
		log.error(message);
	}
	
	public DaoException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}
	
	public DaoException(Throwable cause) {
		super(cause);
		log.error(MESSAGE, cause);
	}
}
