package by.bsuir.hypermarket.entity;

import java.io.Serializable;

/**
 * A base class for defining new concrete entities.
 * @author Raman
 *
 */
public class Entity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3654875736838252745L;
	private int uid;

	/**
	 * @return the id - unique entity id
	 */
	public int getUid() {
		return uid;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Entity [uid=");
		builder.append(uid);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @param id the unique entity id to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + uid;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (uid != other.uid)
			return false;
		return true;
	}
}
