package by.bsuir.hypermarket.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Goods extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8581576043148028193L;
	private String barCode;
	private String name;
	private BigDecimal price;
	private int quantity;
	private Department department;
	private Date madeDate;
	private int discount;
	private AgeLimit ageLimit;
	private GoodsCondition goodsCondition;
	private Country country;
	private int shelfLife;
	private Producer producer;
	private Category category;
	
	/**
	 * @return the barCode
	 */
	public String getBarCode() {
		return barCode;
	}
	/**
	 * @param barCode the barCode to set
	 */
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}
	/**
	 * @return the madeDate
	 */
	public Date getMadeDate() {
		return madeDate;
	}
	/**
	 * @param madeDate the madeDate to set
	 */
	public void setMadeDate(Date madeDate) {
		this.madeDate = madeDate;
	}
	/**
	 * @return the discount
	 */
	public int getDiscount() {
		return discount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	/**
	 * @return the ageLimit
	 */
	public AgeLimit getAgeLimit() {
		return ageLimit;
	}
	/**
	 * @param ageLimit the ageLimit to set
	 */
	public void setAgeLimit(AgeLimit ageLimit) {
		this.ageLimit = ageLimit;
	}
	/**
	 * @return the goodsCondition
	 */
	public GoodsCondition getGoodsCondition() {
		return goodsCondition;
	}
	/**
	 * @param goodsCondition the goodsCondition to set
	 */
	public void setGoodsCondition(GoodsCondition goodsCondition) {
		this.goodsCondition = goodsCondition;
	}
	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}
	/**
	 * @return the shelfLife
	 */
	public int getShelfLife() {
		return shelfLife;
	}
	/**
	 * @param shelfLife the shelfLife to set
	 */
	public void setShelfLife(int shelfLife) {
		this.shelfLife = shelfLife;
	}
	/**
	 * @return the producer
	 */
	public Producer getProducer() {
		return producer;
	}
	/**
	 * @param producer the producer to set
	 */
	public void setProducer(Producer producer) {
		this.producer = producer;
	}
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((ageLimit == null) ? 0 : ageLimit.hashCode());
		result = prime * result + ((barCode == null) ? 0 : barCode.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((department == null) ? 0 : department.hashCode());
		result = prime * result + discount;
		result = prime * result
				+ ((goodsCondition == null) ? 0 : goodsCondition.hashCode());
		result = prime * result
				+ ((madeDate == null) ? 0 : madeDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result
				+ ((producer == null) ? 0 : producer.hashCode());
		result = prime * result + quantity;
		result = prime * result + shelfLife;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Goods other = (Goods) obj;
		if (ageLimit == null) {
			if (other.ageLimit != null)
				return false;
		} else if (!ageLimit.equals(other.ageLimit))
			return false;
		if (barCode == null) {
			if (other.barCode != null)
				return false;
		} else if (!barCode.equals(other.barCode))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (discount != other.discount)
			return false;
		if (goodsCondition == null) {
			if (other.goodsCondition != null)
				return false;
		} else if (!goodsCondition.equals(other.goodsCondition))
			return false;
		if (madeDate == null) {
			if (other.madeDate != null)
				return false;
		} else if (!madeDate.equals(other.madeDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (producer == null) {
			if (other.producer != null)
				return false;
		} else if (!producer.equals(other.producer))
			return false;
		if (quantity != other.quantity)
			return false;
		if (shelfLife != other.shelfLife)
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Goods [barCode=");
		builder.append(barCode);
		builder.append(", name=");
		builder.append(name);
		builder.append(", price=");
		builder.append(price);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append(", department=");
		builder.append(department);
		builder.append(", madeDate=");
		builder.append(madeDate);
		builder.append(", discount=");
		builder.append(discount);
		builder.append(", ageLimit=");
		builder.append(ageLimit);
		builder.append(", goodsCondition=");
		builder.append(goodsCondition);
		builder.append(", country=");
		builder.append(country);
		builder.append(", shelfLife=");
		builder.append(shelfLife);
		builder.append(", producer=");
		builder.append(producer);
		builder.append(", category=");
		builder.append(category);
		builder.append(", getUid()=");
		builder.append(getUid());
		builder.append("]");
		return builder.toString();
	}


}
