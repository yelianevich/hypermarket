package by.bsuir.hypermarket.exception;

// TODO не использовано (в командах обрабатываются DAOException) ?? 
/**
 * HypermarketException thrown if lower level exceptions could not be handled
 * @author Raman
 *
 */
public class HypermarketException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7846932447271292171L;

	public HypermarketException() {
		super();
	}
	
	public HypermarketException(String message) {
		super(message);
	}
	
	public HypermarketException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public HypermarketException(Throwable cause) {
		super(cause);
	}
}
