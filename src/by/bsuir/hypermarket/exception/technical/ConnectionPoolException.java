package by.bsuir.hypermarket.exception.technical;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.exception.HypermarketException;
import by.bsuir.hypermarket.manager.MessageManager;

public class ConnectionPoolException extends HypermarketException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6890070980400263853L;
	private final Logger log = Logger.getLogger(getClass().getSimpleName());

	public ConnectionPoolException() {
		log.error(MessageManager.INSTANCE.getProperty(MessageManager.CONNECTION_POOL_EXCEPTION));
	}
	
	public ConnectionPoolException(String message) {
		super(message);
		log.error(message);
	}
	
	public ConnectionPoolException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}
	
	public ConnectionPoolException(Throwable cause) {
		super(cause);
		log.error(MessageManager.INSTANCE.getProperty(MessageManager.CONNECTION_POOL_EXCEPTION), cause);
	}
}
