package by.bsuir.hypermarket.exception.technical;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.exception.HypermarketException;

public class DAOException extends HypermarketException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1202281702878971943L;
	private final Logger log = Logger.getLogger(getClass().getSimpleName());
	private static final String MESSAGE = "Error during access to object's data";
	
	public DAOException() {
		log.error(MESSAGE);
	}
	
	public DAOException(String message) {
		super(message);
		log.error(message);
	}
	
	public DAOException(String message, Throwable cause) {
		super(message, cause);
		log.error(message, cause);
	}
	
	public DAOException(Throwable cause) {
		super(cause);
		log.error(MESSAGE, cause);
	}
}
