package by.bsuir.hypermarket.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Filter for incoming requests from the client
 * @author Raman
 *
 */
@WebFilter (filterName = "AccessFilter")
public class AccessFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException { }

	/**
	 * Filters incoming requests and do not allow access to the resources
	 * with a command to unauthenticated and/or unauthorized users.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		AccessWrapper requestWrapper = new AccessWrapper((HttpServletRequest) request);
		chain.doFilter(requestWrapper, response);
	}

	@Override
	public void destroy() { }
}
