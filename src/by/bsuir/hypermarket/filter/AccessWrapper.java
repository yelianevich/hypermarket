package by.bsuir.hypermarket.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.entity.User;

/**
 * Wrapper for original request.
 * getParameter method is overrode and it checks security 
 * settings and return appropriate parameter to the servlet.
 * @author Raman
 *
 */
public class AccessWrapper extends HttpServletRequestWrapper {

	public AccessWrapper(HttpServletRequest request) {
		super(request);
	}
	
	/**
	 * Filters command parameters. Does not allow command access without 
	 * appropriate rights
	 */
	@Override
	public String getParameter(String param) {
		HttpSession session = super.getSession(false);
		String command = super.getParameter("command");
		if (session != null) {
			User user = (User) session.getAttribute("user");
			if ((user == null) && (!"login".equals(command))) {
				return null;
			} else if ((Commands.SHOW_USERS_COMMAND.equals(command)) && (!user.getIsAdmin())) {
				return Commands.HOMEPAGE_COMMAND;
			} else if ((command == null) && ("command".equals(param))) {
				return Commands.HOMEPAGE_COMMAND;
			}
		} else if (!"login".equals(command)){
			return null;
		}
		return super.getParameter(param);
	}
}
