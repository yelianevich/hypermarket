package by.bsuir.hypermarket.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * Guarantee correct encoding interpretation 
 * @author Raman
 *
 */
@WebFilter(
		filterName = "EncodingFilter",
		initParams = @WebInitParam(name = "encoding", value = "UTF-8")
)
public class EncodingFilter implements Filter {
	private static final String ENCODING_DEFAULT = "UTF-8";
	private static final String ENCODING_PARAM_NAME = "encoding";
	private String encoding;
	
	@Override
	public void destroy() {	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException { 
		encoding = filterConfig.getInitParameter(ENCODING_PARAM_NAME);
		if (encoding == null) {
			encoding = ENCODING_DEFAULT;
		}
	}
	
}
