package by.bsuir.hypermarket.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import by.bsuir.hypermarket.entity.User;

/**
 * Wrapper for original request.
 * getParameter method is overrided and it checks security 
 * settings and return appropriate parameter to the servlet.
 * @author Raman
 *
 */
public class RequestWrapper extends HttpServletRequestWrapper {

	public RequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String getParameter(String param) {
		HttpSession session = super.getSession(false);
		String command = super.getParameter("command");
		System.out.println(command);
		if (session != null) {
			User user = (User) session.getAttribute("user");
			if ((user == null) && (!"login".equals(command))) {
				System.out.println("no user");
				return null;
			} else if ((command == null) && ("command".equals(param))) {
				return "homepage";
			}
		} else if (!"login".equals(command)){
			System.out.println("no session and not trying to login");
			return null;
		}
		return super.getParameter(param);
	}
}
