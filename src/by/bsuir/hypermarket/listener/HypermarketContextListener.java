package by.bsuir.hypermarket.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;
import by.bsuir.hypermarket.manager.ConfigurationManager;

/**
 * Initialize web application - database connection pool, log4j - and releases their resources
 * @author Raman
 *
 */
@WebListener(value = "Initialize database connection pool, log4j and releases their resources")
public class HypermarketContextListener implements ServletContextListener {
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		try {
			DbConnectionPool.INSTANCE.releaseResources();
		} catch (ConnectionPoolException e){
			Logger log = Logger.getLogger(getClass().getSimpleName());
			log.error("Error during releasing resources", e);
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String contextPath = sce.getServletContext().getRealPath("/");
		String separator = System.getProperty("file.separator");
		String file = "WEB-INF" + separator + "classes" + separator + "resources" + separator + "log4j.properties";
		System.setProperty("logsPath", contextPath + "WEB-INF" + separator + "logs" + separator);
		PropertyConfigurator.configure(contextPath + file);
		Logger log = Logger.getLogger(getClass().getSimpleName());
		log.debug(contextPath);
		try {
			try {
				DbConnectionPool.INSTANCE.init(Integer.parseInt(ConfigurationManager.INSTANCE.getProperty(ConfigurationManager.POOL_CAPACITY)));
			} catch (NumberFormatException e) {
				log.error("Error during parsing value from configuration file. Pool initialized with default capacity", e);
				DbConnectionPool.INSTANCE.init(DbConnectionPool.DEFAULT_CAPACITY);
			}
		} catch (ConnectionPoolException e) {
			log.error("Error during initializing connection pool", e);
		}
	}

}
