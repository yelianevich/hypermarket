package by.bsuir.hypermarket.logic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.bsuir.hypermarket.connection.DBConnection;
import by.bsuir.hypermarket.connection.DBConnectionPool;
import by.bsuir.hypermarket.exception.technical.ConnectionPoolException;
import by.bsuir.hypermarket.security.SHA1;

public class LoginLogic {
	private static final String SEARCH_USER = "SELECT * FROM USERS WHERE u_login = ? AND u_password = ?";
	private static final Logger LOG = Logger.getLogger("Login Logic");
	
	/**
	 * Checks user's login and password.
	 * @param login - user's login
	 * @param password - user's password
	 * @return true - user is registered, false otherwise
	 */
	public static boolean checkLogin(String login, String password) {
		
		try (DBConnection connection = DBConnectionPool.INSTANCE.getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(SEARCH_USER)) {
				String passwordDigest = SHA1.getHash(password.getBytes());
				statement.setString(1, login);
				statement.setString(2, passwordDigest);
				try (ResultSet resultSet = statement.executeQuery()) {					
					return resultSet.next();
				}
			} catch (SQLException e) {
				LOG.error("Was unable to check login and password", e);
				return false;
			}
		} catch (ConnectionPoolException e) {
			LOG.error("Was unable to check login and password", e);
			return false;
		}
	}
}
