package by.bsuir.hypermarket.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Singleton class to localize messages for user
 * @author Raman
 *
 */
public enum MessageManager {
	INSTANCE;

	private static final String BUNDLE_NAME = "resources.messages";
	public static final String LOGIN_ERROR_MESSAGE = "LOGIN_ERROR_MESSAGE";
	public static final String DAO_ERROR_MESSAGE = "DAO_ERROR_MESSAGE";
	public static final String SERVLET_EXCEPTION_ERROR_MESSAGE = "SERVLET_EXCEPTION_ERROR_MESSAGE";
	public static final String IO_EXCEPTION_ERROR_MESSAGE = "IO_EXCEPTION_ERROR_MESSAGE";
	public static final String CONNECTION_POOL_EXCEPTION = "CONNECTION_POOL_EXCEPTION";
	public static final String LOGIN_TITLE = "LOGIN_TITLE";
	public static final String SHOW_USERS_MESSAGE = "ShowUsers";
	public static final String USER_BANNED_MESSAGE = "YouAreBanned";
	
	private ResourceBundle resourceBundle;

	private MessageManager() {
		this.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME,
				Locale.getDefault());
	}

	public void setLocale(Locale locale) {
		this.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
	}

	/**
	 * Retrieves value from properties file according to the input key  
	 * @param key defines value in the properties file  
	 * @return value from the properties file
	 */
	public String getProperty(String key) {
		return this.resourceBundle.getString(key);
	}
}
