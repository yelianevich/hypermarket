package by.bsuir.hypermarket.request;

// TODO новый интерфейс для wrapper
/**
 * Defines operations with commands parameters.
 * @author Raman
 *
 * @param <T> - type of the parameters source
 */
public interface CommandParams <T> {
	
	/**
	 * Sets underlying object with parameters
	 * @param commandsParams object with parameters
	 */
	public void setCommandParamsSource(T commandsParams);
	
	/**
	 * @return underlying parameters source object
	 */
	public T getCommandParamsSource();
	
	/**
	 * Gets parameter with specified name
	 * @param name - parameter name
	 * @return parameter value
	 */
	public String getParameter(String name);
	
	/**
	 * Gets attribute with specified name
	 * @param name - parameter name
	 * @return parameter value
	 */
	public Object getAttribute(String name);
	
	/**
	 * Sets attribute value for specified name
	 * @param name attribute name
	 * @param value attribute value
	 */
	public void setAttribute(String name, Object value);
	
	/**
	 * Removes attribute with specified name
	 * @param name attribute name
	 */
	public void removeAttribute(String name);
	
	/**
	 * Creates session for underlying source object
	 */
	public void createSession();
	
	/**
	 * Destroys session for underlying source object
	 */
	public void destroySession();
	
	/**
	 * Gets session attribute with specified name
	 * @param name - session parameter name
	 * @return session parameter value
	 */
	public Object getSessionAttribute(String name);
	
	/**
	 * Sets session attribute value for specified name
	 * @param name session attribute name
	 * @param value session attribute value
	 */
	public void setSessionAttribute(String name, Object value);
}