package by.bsuir.hypermarket.request;

import javax.servlet.http.HttpServletRequest;

// TODO новый wrapper
public class RequestCommandsParams implements CommandParams <HttpServletRequest> {
	private HttpServletRequest request;
	
	@Override
	public String getParameter(String name) {
		return this.request.getParameter(name);
	}

	@Override
	public Object getAttribute(String name) {
		return this.request.getAttribute(name);
	}

	@Override
	public void setAttribute(String name, Object value) {
		this.request.setAttribute(name, value);
	}
	
	@Override
	public void removeAttribute(String name) {
		this.request.removeAttribute(name);
	}

	@Override
	public Object getSessionAttribute(String name) {
		return this.request.getSession().getAttribute(name);
	}

	@Override
	public void setSessionAttribute(String name, Object value) {
		this.request.getSession().setAttribute(name, value);
	}

	@Override
	public HttpServletRequest getCommandParamsSource() {
		return this.request;
	}

	@Override
	public void setCommandParamsSource(HttpServletRequest commandsParams) {
		this.request = commandsParams;
	}

	@Override
	public void destroySession() {
		this.request.getSession().invalidate();
	}

	@Override
	public void createSession() {
		this.request.getSession();
	}
}
