package by.bsuir.hypermarket.request;

import java.util.HashMap;
import java.util.Map;

import by.bsuir.hypermarket.command.Command;
import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.command.impl.AboutChainCommand;
import by.bsuir.hypermarket.command.impl.AboutCommand;
import by.bsuir.hypermarket.command.impl.CatalogCommand;
import by.bsuir.hypermarket.command.impl.ChangeLanguageCommand;
import by.bsuir.hypermarket.command.impl.HomepageCommand;
import by.bsuir.hypermarket.command.impl.InvertBanCommand;
import by.bsuir.hypermarket.command.impl.LoginCommand;
import by.bsuir.hypermarket.command.impl.LogoutCommand;
import by.bsuir.hypermarket.command.impl.NoCommand;
import by.bsuir.hypermarket.command.impl.ShowGoodsCommand;
import by.bsuir.hypermarket.command.impl.ShowUsersList;

/**
 * Singleton class. Helps to get command class according to the request {@link Commands#COMMAND} parameter.
 * @author Raman
 *
 */
public enum RequestHelper {
	INSTANCE;
	
	private Map<String, Command> commands;
	
	private RequestHelper() {
		commands = new HashMap<String, Command>();
		commands.put(Commands.LOGIN_COMMAND, new LoginCommand());
		commands.put(Commands.SHOW_GOODS_COMMAND, new ShowGoodsCommand());
		commands.put(Commands.HOMEPAGE_COMMAND, new HomepageCommand());
		commands.put(Commands.CATALOG_COMMAND, new CatalogCommand());
		commands.put(Commands.ABOUT_COMMAND, new AboutCommand());
		commands.put(Commands.ABOUT_CHAIN, new AboutChainCommand());
		commands.put(Commands.LOGOUT_COMMAND, new LogoutCommand());
		commands.put(Commands.CHANGE_LANG_COMMAND, new ChangeLanguageCommand());
		commands.put(Commands.SHOW_USERS_COMMAND, new ShowUsersList());
		commands.put(Commands.INVERT_BAN_COMMAND, new InvertBanCommand());
	}

	/**
	 * Determine command in the command parameters and return concrete
	 * Command implementation
	 * @param request
	 * @return
	 */
	public Command getCommand(CommandParams<?> request) {
		String action = request.getParameter(Commands.COMMAND);
		Command command = commands.get(action);
		if (command == null) {
			command = new NoCommand();
		}
		return command;
	}
}
