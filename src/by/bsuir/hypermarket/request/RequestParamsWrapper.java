package by.bsuir.hypermarket.request;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import by.bsuir.hypermarket.command.Commands;

// TODO delete (старый wrapper)?
public class RequestParamsWrapper extends HttpServletRequestWrapper {
	private Map<String, String> params = new HashMap<>();
	
	public RequestParamsWrapper(HttpServletRequest request) {
		super(request);
		fillUpMaps(request);
	}
	
	private final void fillUpMaps(HttpServletRequest request) {	
		params.put(Commands.COMMAND, super.getParameter(Commands.COMMAND));
		params.put(Commands.SHOW_USERS_COMMAND, super.getParameter(Commands.SHOW_USERS_COMMAND));
		params.put(Commands.CHANGE_LANG_LANG, super.getParameter(Commands.CHANGE_LANG_LANG));
		params.put(Commands.LOGIN_LOGIN, super.getParameter(Commands.LOGIN_LOGIN));
		params.put(Commands.LOGIN_PASSWORD, super.getParameter(Commands.LOGIN_PASSWORD));
		params.put(Commands.SHOW_GOODS_COMMAND, super.getParameter(Commands.SHOW_GOODS_COMMAND));
		params.put(Commands.SHOW_GOODS_CATEGOTY, super.getParameter(Commands.SHOW_GOODS_CATEGOTY));
		params.put(Commands.SHOW_GOODS_DEPARTMENT, super.getParameter(Commands.SHOW_GOODS_DEPARTMENT));
		params.put(Commands.INVERT_BAN_USER_UID, super.getParameter(Commands.INVERT_BAN_USER_UID));
	}
	
	@Override
	public String getParameter(String name) {	
		return params.get(name);
	}
}
