package by.bsuir.hypermarket.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import by.bsuir.hypermarket.entity.Department;

public class CatalogTag extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8196680602642834937L;
	private List<Department> departments = new ArrayList<>();
	
	@Override
	public int doStartTag() throws JspException {
		/*JspWriter jspWriter = pageContext.getOut();
		try {	
			if (role.compareToIgnoreCase("admin") == 0) {
				jspWriter.write("<h1>Admin - " + login + "</h1>");
			} else {
				jspWriter.write("<h1>User - " + login + "</h1>");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;*/
		JspWriter jspWriter = pageContext.getOut();
		try {
			jspWriter.write("<table class=\"catalogTable\" cellspacing=\"5\" cellpadding=\"0\" border=\"0\" >");
			
			jspWriter.write("<table class=\"catalogTable\">");
		} catch (IOException e) {
			throw new JspException(e);
		}
		
		return SKIP_BODY;
	}

	/**
	 * @return the departments
	 */
	public List<Department> getDepartments() {
		return departments;
	}

	/**
	 * @param departments the departments to set
	 */
	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}
}
