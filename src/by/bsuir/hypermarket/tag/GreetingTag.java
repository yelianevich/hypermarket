package by.bsuir.hypermarket.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class GreetingTag extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8196680602642834937L;
	private String login;
	private String role;
	
	@Override
	public int doStartTag() throws JspException {
		JspWriter jspWriter = pageContext.getOut();
		try {	
			if (role.compareToIgnoreCase("admin") == 0) {
				jspWriter.write("<h1>Admin - " + login + "</h1>");
			} else {
				jspWriter.write("<h1>User - " + login + "</h1>");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
}
