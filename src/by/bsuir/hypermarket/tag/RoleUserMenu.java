package by.bsuir.hypermarket.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import by.bsuir.hypermarket.command.Commands;
import by.bsuir.hypermarket.entity.User;
import by.bsuir.hypermarket.manager.MessageManager;

/**
 * Filter, that generate menu according to the user role 
 * @author Raman
 *
 */
public class RoleUserMenu extends SimpleTagSupport {
	private User user;
	
	@Override
	public void doTag() throws JspException, IOException {
		if (user.getIsAdmin()) {
			getJspContext().setAttribute(Commands.SHOW_USERS_TAG_MENU_COMMAND, Commands.SHOW_USERS_COMMAND); 
			getJspContext().setAttribute(Commands.SHOW_USERS_TAG_MENU_LABEL, MessageManager.SHOW_USERS_MESSAGE);
			getJspBody().invoke(null);
		}
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
