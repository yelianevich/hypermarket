package by.bsuir.hypermarket.connection;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.exception.ConnectionPoolException;

/**
 * In config.properties url to the database should be changed to DB_URL = jdbc:mysql://localhost:3306/aoba_balance_lab1
 * @author Raman
 *
 */
public class DBConnectionPoolTest {
	private static final String BALANCE_QUERY = "SELECT b_price, b_quantity, b_balance, b_date FROM balance";
	private static final int CONNECTIONS_NUM = 5;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(CONNECTIONS_NUM);
	}

	@Test
	public void test() {
		List<Thread> threads = new LinkedList<Thread>();
		
		for (int i = 0; i < CONNECTIONS_NUM * 10; ++i) {
			threads.add(new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println(Thread.currentThread().getName() + "is running");
					try (DbConnection con = DbConnectionPool.INSTANCE.getConnection();
						 Statement statement = con.createStatement(); 
						 ResultSet result = statement.executeQuery(BALANCE_QUERY);) {
								while (result.next()) {
									int quantity = result.getInt("b_quantity");
									BigDecimal price = result.getBigDecimal("b_price");
									BigDecimal balance = result.getBigDecimal("b_balance");
									Date date = result.getDate("b_date");
	
									System.out.println(Thread.currentThread().getName() + ": quantity = " + quantity +
											"; price = " + price + 
											"; balance = " + balance + 
											"; date = " + date);
									Thread.sleep(500);
								}
					} catch (ConnectionPoolException | InterruptedException | SQLException e) {
						e.printStackTrace();
					}
				}
			}));
		}
		for (Thread t : threads) {
			t.start();
		}
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	@After
	public void tearDown() {
		try {
			DbConnectionPool.INSTANCE.releaseResources();
		} catch (ConnectionPoolException e) {
			e.printStackTrace();
		}
	}
}
