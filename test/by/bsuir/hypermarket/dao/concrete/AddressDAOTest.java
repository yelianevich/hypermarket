package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Address;

public class AddressDaoTest {
	private AddressDao addressDao;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		addressDao = new AddressDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Address> addresses = addressDao.findAll();
			for (Address a : addresses) {
				System.out.println(a);
			}
			System.out.println("findEntityById");
			System.out.println(addressDao.findEntityById(1));
			System.out.println(addressDao.findEntityById(2));
			System.out.println("findProducerAddresses");
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}

}
