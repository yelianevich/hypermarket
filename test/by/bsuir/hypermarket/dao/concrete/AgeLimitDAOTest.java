package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.AgeLimit;

public class AgeLimitDaoTest {

	private AgeLimitDao ageLimit;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		ageLimit = new AgeLimitDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<AgeLimit> ageLimits = ageLimit.findAll();
			for (AgeLimit c : ageLimits) {
				System.out.println(c);
			}
			System.out.println("findEntityById");
			System.out.println(ageLimit.findEntityById(1));
			System.out.println(ageLimit.findEntityById(2));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}

}
