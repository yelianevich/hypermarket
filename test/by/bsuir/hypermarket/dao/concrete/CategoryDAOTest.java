package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Category;

public class CategoryDaoTest {
	private CategoryDao categoryDao;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		categoryDao = new CategoryDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Category> categoriess = categoryDao.findAll();
			for (Category c : categoriess) {
				System.out.println(c);
			}
			System.out.println("findEntityById");
			System.out.println(categoryDao.findEntityById(1));
			System.out.println(categoryDao.findEntityById(2));
			System.out.println(categoryDao.findEntityById(3));
			System.out.println(categoryDao.findEntityById(4));
			System.out.println(categoryDao.findEntityById(5));
			System.out.println(categoryDao.findEntityById(6));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}
}
