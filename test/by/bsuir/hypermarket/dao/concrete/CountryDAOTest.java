package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Country;

public class CountryDaoTest {
	private CountryDao countryDAO;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		countryDAO = new CountryDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Country> countries = countryDAO.findAll();
			for (Country c : countries) {
				System.out.println(c);
			}
			System.out.println("findEntityById");
			System.out.println(countryDAO.findEntityById(1));
			System.out.println(countryDAO.findEntityById(2));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}

}
