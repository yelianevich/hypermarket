package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Department;

public class DepartmentDaoTest {

	private DepartmentDao departmentsDAO;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		departmentsDAO = new DepartmentDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Department> departments = departmentsDAO.findAll();
			for (Department d : departments) {
				System.out.println(d);
			}
			System.out.println("findEntityById");
			System.out.println(departmentsDAO.findEntityById(1));
			System.out.println(departmentsDAO.findEntityById(2));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}
}
