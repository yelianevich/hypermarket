package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.GoodsCondition;

public class GoodsConditionDaoTest {
	private GoodsConditionDao goodsConditionDAO;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		goodsConditionDAO = new GoodsConditionDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<GoodsCondition> goodsConditions = goodsConditionDAO.findAll();
			for (GoodsCondition c : goodsConditions) {
				System.out.println(c);
			}
			System.out.println("findEntityById");
			System.out.println(goodsConditionDAO.findEntityById(1));
			System.out.println(goodsConditionDAO.findEntityById(2));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}

}
