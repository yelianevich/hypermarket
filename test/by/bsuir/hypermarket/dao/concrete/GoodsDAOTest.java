package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Goods;

public class GoodsDaoTest {
	private GoodsDao goodsDAO;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		goodsDAO = new GoodsDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Goods> goodsList = goodsDAO.findAll();
			for (Goods g : goodsList) {
				System.out.println(g);
			}
			System.out.println("findEntityById");
			System.out.println(goodsDAO.findEntityById(1));
			System.out.println(goodsDAO.findEntityById(2));
			
			System.out.println("findEntityByCategory");
			System.out.println(goodsDAO.findEntitiesByCategory(1));
			System.out.println(goodsDAO.findEntitiesByCategory(5));
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

}
