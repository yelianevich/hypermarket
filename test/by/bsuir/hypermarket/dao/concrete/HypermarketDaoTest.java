package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.Hypermarket;

public class HypermarketDaoTest {

private HypermarketDao hypermarketDao;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		hypermarketDao = new HypermarketDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<Hypermarket> hypermarkets = hypermarketDao.findAll();
			for (Hypermarket c : hypermarkets) {
				System.out.println(c);
			}
			System.out.println("findEntityById");
			System.out.println(hypermarketDao.findEntityById(1));
			System.out.println(hypermarketDao.findEntityById(2));
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}
}
