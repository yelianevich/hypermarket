package by.bsuir.hypermarket.dao.concrete;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DbConnectionPool;
import by.bsuir.hypermarket.dao.exception.DaoException;
import by.bsuir.hypermarket.entity.User;

public class UserDaoTest {
	private UserDao userDao;
	
	@Before
	public void setUp() throws Exception {
		DbConnectionPool.INSTANCE.init(1);
		userDao = new UserDao();
	}

	@After
	public void tearDown() throws Exception {
		DbConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		try {
			System.out.println("findAll");
			List<User> users = userDao.findAll();
			for (User p : users) {
				System.out.println(p);
			}
			
			User user = new User();
			user.setBan(false);
			user.setIsAdmin(false);
			user.setLogin("useri");
			user.setUid(3);
			
			User prevUser = userDao.updateEntity(user);
			
			System.out.println("Find entity by id test");
			System.out.println("1 user: " + userDao.findEntityById(1));
			System.out.println("2 user: " + userDao.findEntityById(2));
			
			System.out.println("Update test");
			System.out.println("New user: " + user);
			System.out.println("Prev user: " + prevUser);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}	
	}
}
