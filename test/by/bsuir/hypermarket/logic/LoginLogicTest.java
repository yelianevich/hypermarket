package by.bsuir.hypermarket.logic;

import junit.framework.Assert;

import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.connection.DBConnectionPool;
import by.bsuir.hypermarket.manager.ConfigurationManager;

public class LoginLogicTest {

	@Before
	public void setUp() throws Exception {
		PropertyConfigurator.configure(ConfigurationManager.LOG_PROPERTIES_PATH);
		DBConnectionPool.INSTANCE.init(1);
	}

	@After
	public void tearDown() throws Exception {
		DBConnectionPool.INSTANCE.releaseResources();
	}

	@Test
	public void test() {
		Assert.assertEquals(true, LoginLogic.checkLogin("admin", "password"));
	}

}
