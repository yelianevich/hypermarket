package by.bsuir.hypermarket.logic;

import org.junit.Before;
import org.junit.Test;

import by.bsuir.hypermarket.security.SHA1;

public class SHADigestTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		String digest1 = SHA1.getHash("useri".getBytes());
		System.out.println(digest1);
		String digest2 = SHA1.getHash("useri".getBytes());
		System.out.println(digest2);
		System.out.println(digest1.compareTo(digest2) == 0);
	}

}
